module Her
  module Paginate
    module RelationExtension
      def self.included(base)
        base.class_eval do
          def each_page
            return to_enum(:each_page) unless block_given?
            number= current_page
            until (page= (number == current_page) ? self : page(number)).blank?
              yield page
              number+=1
            end
            page
          end
        end
      end
    end
  end
end
