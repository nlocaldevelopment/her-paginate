require "her/paginate/version"
require 'her'
require 'kaminari'

module Her
  module Paginate
    # Your code goes here...
  end
end

require 'her/paginate/collection'
require 'her/paginate/pagination_parser'
require 'her/paginate/relation_extension'
